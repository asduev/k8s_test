### How to run Minikube under Windows

Minikube is a tool that runs a single-node Kubernetes cluster in a virtual machine on PC.
It can be run under Window and Linux.
https://kubernetes.io/docs/setup/learning-environment/minikube/

#### Install chocolatey from Windows admin console
chocolatey is the package manager for Windows. It is needed for easy installation of minikube.

https://chocolatey.org/install

#### Install VirtualBox
Minikube runs on top of VirtualBox, so VirtualBox should be installed.

#### Install minikube and etc

All other software can be installed using chocolatey.

```sh
$ choco install minikube
$ choco install docker-cli
$ choco install kubernetes-helm
```

#### Setup proxy settings
Minikube downloads images from internet. Proxy should be configured to start it.
Minikube API server should be accessible without proxy (NO_PROXY).

```sh
$ set HTTP_PROXY=http://proxy.te.mera.ru:8080
$ set HTTPS_PROXY=http://proxy.te.mera.ru:8080
$ set NO_PROXY=192.168.99.100
```

Commands to set environment variables in Power Shell:

```sh
Set-Item Env:NO_PROXY 192.168.99.100
Set-Item Env:HTTP_PROXY http://proxy.te.mera.ru:8080
Set-Item Env:HTTPS_PROXY http://proxy.te.mera.ru:8080
```

Since sometimes in consequent attempts to start minikuber API server address may be stepped, NO_PROXY can be configured to range of addresses

```sh
Set-Item Env:NO_PROXY 192.168.99.0/24
```

#### Run minikube cluster
By default minikube uses 2 GB RAM and 1 CPU.
```sh
$ minikube start
```

If minikube tries to use hyperv by default, virtualbox can be forcely chosen

```sh
minikube start  --vm-driver=virtualbox
```

#### Setup Docker env
Minikube includes docker daemon, so it's possible to build your own image inside minikube cluster.

Docker cli should be configured to access docker daemon:

```sh
$ minikube docker-env
SET DOCKER_TLS_VERIFY=1
SET DOCKER_HOST=tcp://192.168.99.100:2376
SET DOCKER_CERT_PATH=C:\Users\aduev\.minikube\certs
REM Run this command to configure your shell:
REM @FOR /f "tokens=*" %i IN ('minikube docker-env') DO @%i
```
Run command from output:
```sh
$ @FOR /f "tokens=*" %i IN ('minikube docker-env') DO @%i
```
#### Build docker image
Kubernetes can download images from internet if you specify them in yaml file.
If you need run yor own software or script you should be built your own image:
 - Create docker file
 - Build image
```sh
$ docker build --tag tls_echo --build-arg https_proxy=http://proxy.te.mera.ru:8080 --build-arg http_proxy=http://proxy.te.mera.ru:8080 .
```

If all environment variables are set correctly and image built passed, your image will be available for docker inside minikube VM

```
> minikube ssh
                          _             _
            _         _ ( )           ( )
  ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __
/' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
| ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
(_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)

$ docker image ls
REPOSITORY                                TAG                 IMAGE ID            CREATED             SIZE
tls_echo                                  latest              034fc10de01b        24 seconds ago      921MB
...

```

#### Deploy your application in kubernetes
To run your application in kubernetes you should create yaml file to specify deployment.

The official documentation is very good. It's recommended to read it first:

https://kubernetes.io/docs/concepts/

When you're ready with yaml file run your pod/pods:
```sh
$ kubectl apply -f internal_client.yaml
```

#### Troubleshooting
It's possible to open minikube dashboard in browser to check errors and check/edit current configuration YAMLs:

```sh
$ minikube dashboard
```

There is a way to login inside pod and run shell:

```sh
aduev@N104149 C:\local\Work\proj\k8s_tls_echo
$ kubectl get pod   echoclient-deployment-54dcdddb4c-gbknw
NAME                                     READY   STATUS    RESTARTS   AGE
echoclient-deployment-54dcdddb4c-gbknw   1/1     Running   0          7m16s

aduev@N104149 C:\local\Work\proj\k8s_tls_echo
$ kubectl exec -it echoclient-deployment-54dcdddb4c-gbknw -- /bin/sh
```

Also container 'stdout' logs can be requested via 'docker' API inside of minikube VM

```
> minikube ssh
                         _             _
            _         _ ( )           ( )
  ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __
/' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
| ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
(_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)

$ docker ps
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS               NAMES
e463303a8f6d        034fc10de01b           "python -u source.py"    5 minutes ago       Up 5 minutes                            k8s_hello_hello-5d6c58f459-
...

$ docker logs e463303a8f6d
======== Running on http://0.0.0.0:5858 ========
(Press CTRL+C to quit)
received request
Request:
<Request POST /chat >
```